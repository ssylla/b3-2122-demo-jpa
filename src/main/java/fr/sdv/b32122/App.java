package fr.sdv.b32122;

import fr.sdv.b32122.bo.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory( "sdv-demo-jpa" );
        EntityManager em = emf.createEntityManager();
        
        //Création d'un nouvel objet
        em.getTransaction().begin();
        // Cinema cinemaACreer = new Cinema( "UGC Paris", "BD Louise Michel");
        // cinemaACreer.addSalle( new Salle( "A07", 55) );
        //
        // System.out.println("Création UGC Paris");
        // em.persist( cinemaACreer );
    
        System.out.println("Modification de l'existant");
        //Mise à jour de l'objet
        //Recherche en base
        Cinema cinema = em.find( Cinema.class, 1L );
        if (null != cinema) {
            System.out.println(cinema);
    
            for ( Salle item : cinema.getSalles() ) {
                System.out.println(item);
            }
            //Suppression
            // em.remove( cinema );
        }
    
        NamedQuery query = ( NamedQuery ) em.createNamedQuery( "findByName", Cinema.class );
        //Création d'un nouveau film
        // Film ts = new Film( "Toys' Story", 90, Genre.FICTION, new Realisateur("sega", "sylla") );
        //
        // em.persist( ts );
        
        em.getTransaction().commit();
        em.close();
        emf.close();
    }
}
