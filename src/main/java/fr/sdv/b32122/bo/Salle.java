package fr.sdv.b32122.bo;

import java.util.Set;
import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Salle implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String code;
	private int nbPlaces;
	
	@ManyToOne
	@JoinColumn(name="id_c")
	private Cinema cinema;
	
	@ManyToMany(mappedBy = "salles")
	private Set<Film> films;
	
	public Salle() {}
	
	public Salle( String code, int nbPlaces ) {
		this.code = code;
		this.nbPlaces = nbPlaces;
	}
	
	public Salle( String code, int nbPlaces, Cinema cinema ) {
		this.code = code;
		this.nbPlaces = nbPlaces;
		this.cinema = cinema;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode( String code ) {
		this.code = code;
	}
	
	public int getNbPlaces() {
		return nbPlaces;
	}
	
	public void setNbPlaces( int nbPlaces ) {
		this.nbPlaces = nbPlaces;
	}
	
	public Cinema getCinema() {
		return cinema;
	}
	
	public void setCinema( Cinema cinema ) {
		if (this.cinema != null) {
			this.cinema.getSalles().remove( this );
		}
		this.cinema = cinema;
		if (this.cinema != null) {
			this.cinema.getSalles().add( this );
		}
	}
	
	public Set<Film> getFilms() {
		return films;
	}
	
	public void setFilms( Set<Film> films ) {
		this.films = films;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Salle{" );
		sb.append( "id=" ).append( id );
		sb.append( ", code='" ).append( code ).append( '\'' );
		sb.append( ", nbPlaces=" ).append( nbPlaces );
		sb.append( '}' );
		return sb.toString();
	}
}
