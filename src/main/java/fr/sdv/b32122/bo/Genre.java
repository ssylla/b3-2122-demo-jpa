package fr.sdv.b32122.bo;

public enum Genre {
	ACTION ("Action"), FICTION("Science fiction");
	
	private String libelle;
	
	Genre( String libelle ) {
		this.libelle = libelle;
	}
	
	public String getLibelle() {
		return libelle;
	}
	
	public void setLibelle( String libelle ) {
		this.libelle = libelle;
	}
}
