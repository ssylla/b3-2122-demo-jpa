package fr.sdv.b32122.bo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@NamedQuery( name = "findByName", query = "SELECT c FROM Cinema c WHERE c.nom = :name")
public class Cinema implements Serializable {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;
	private String nom;
	@Column( name = "adresse_complete", length = 500 )
	private String adresse;
	@Transient
	private String test;
	
	@OneToMany( mappedBy = "cinema", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER )
	private Set<Salle> salles;
	
	{
		salles = new HashSet<>();
	}
	
	public Cinema() {}
	
	public Cinema( String nom, String adresse ) {
		this.nom = nom;
		this.adresse = adresse;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom( String nom ) {
		this.nom = nom;
	}
	
	public String getAdresse() {
		return adresse;
	}
	
	public void setAdresse( String adresse ) {
		this.adresse = adresse;
	}
	
	public String getTest() {
		return test;
	}
	
	public void setTest( String test ) {
		this.test = test;
	}
	
	public Set<Salle> getSalles() {
		return salles;
	}
	
	public void setSalles( Set<Salle> salles ) {
		this.salles = salles;
	}
	
	public void addSalle( Salle salle ) {
		salle.setCinema( this );
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Cinema{" );
		sb.append( "id=" ).append( id );
		sb.append( ", nom='" ).append( nom ).append( '\'' );
		sb.append( ", addresse='" ).append( adresse ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
