package fr.sdv.b32122.bo;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Realisateur implements Serializable {
	
	
	@Column(name = "prenom_realisateur", length = 100, nullable = false)
	private String prenom;
	@Column(name = "nom_realisateur", length = 100, nullable = false)
	private String nom;
	
	public Realisateur() {}
	
	public Realisateur( String nom, String prenom ) {
		this.nom = nom;
		this.prenom = prenom;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom( String nom ) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setPrenom( String prenom ) {
		this.prenom = prenom;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Realisateur{" );
		sb.append( "nom='" ).append( nom ).append( '\'' );
		sb.append( ", prenom='" ).append( prenom ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
