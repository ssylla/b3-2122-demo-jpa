package fr.sdv.b32122.bo;

import java.util.Set;

import javax.persistence.*;

@Entity
public class Film {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String titre;
	private int duree;
	@Enumerated(EnumType.STRING)
	private Genre genre;
	@ManyToMany
	@JoinTable(
			name = "diffusion",
			joinColumns = @JoinColumn(name = "id_film", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_salle", referencedColumnName = "id")
	)
	private Set<Salle> salles;
	
	@Embedded
	private Realisateur realisateur;
	
	public Film() {	}
	
	public Film( String titre, int duree ) {
		this.titre = titre;
		this.duree = duree;
	}
	
	public Film( String titre, int duree, Realisateur realisateur ) {
		this.titre = titre;
		this.duree = duree;
		this.realisateur = realisateur;
	}
	
	public Film( String titre, int duree, Genre genre, Realisateur realisateur ) {
		this.titre = titre;
		this.duree = duree;
		this.genre = genre;
		this.realisateur = realisateur;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getTitre() {
		return titre;
	}
	
	public void setTitre( String titre ) {
		this.titre = titre;
	}
	
	public int getDuree() {
		return duree;
	}
	
	public void setDuree( int duree ) {
		this.duree = duree;
	}
	
	public Set<Salle> getSalles() {
		return salles;
	}
	
	public void setSalles( Set<Salle> salles ) {
		this.salles = salles;
	}
	
	public Realisateur getRealisateur() {
		return realisateur;
	}
	
	public void setRealisateur( Realisateur realisateur ) {
		this.realisateur = realisateur;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Film{" );
		sb.append( "id=" ).append( id );
		sb.append( ", titre='" ).append( titre ).append( '\'' );
		sb.append( ", duree=" ).append( duree );
		sb.append( '}' );
		return sb.toString();
	}
}
